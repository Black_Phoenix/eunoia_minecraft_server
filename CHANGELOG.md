# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.0.0] - 28-03-2021

### Features
- Added Small Ships, a mod that allows crafting ships that act like an advanced version of the vanilla boat.

### Changes
- Tweaked alex mobs spawn frequency
- Adjusted Ice and Fire monster nests (roosts, hydra caves, gorgons...)
- Limited Astral Socery shrine spawns
- Removed unused mods

## [v0.4.0] - 28-03-2021

### Changes
- Removed unused libraries
- Removed mods with very little functionality that added a library on top of intended
- Removed QuarkOddities, and thus, Matrix Enchant feature.

## [v0.3.0] - 25-03-2021

### Changes
- Removed vanilla unfriendly mods

### Known Issues
- Hostile mob spawning not working as intended. Very few enemies spawn.

## [v0.2.0] - 25-03-2021

### Changes
- Removes Valhesia modpack structure. Removed big undesired mods to lower RAM usage and loading times.

## [v0.1.0] - 23-03-2021

### Features
- Implemented [Valhesia 3 1.16.5 modpack]: https://www.curseforge.com/minecraft/modpacks/valhelsia-3
